import ComponenteUno from "./Components/ComponenteUno";
import ComponenteDos from "./Components/ComponenteDos";
import ComponenteTres from "./Components/ComponenteTres";
import ComponenteCuatro from "./Components/ComponenteCuatro";
import ComponenteCinco from "./Components/ComponenteCinco";
import ComponenteSeis from "./Components/ComponenteSeis";
import ComponenteImg from "./Components/ComponenteImg";

function App() {
  return (
    <>
      <div className="principal">
        <ComponenteImg/>
      </div>
      <div className="container1">
        <ComponenteSeis/>
      </div>
      <div className="container2">
        <ComponenteUno/>
        <ComponenteDos/>
        <ComponenteTres/>
      </div>
      <div className="container3">
        <ComponenteCuatro/>
        <ComponenteCinco/>
      </div>
    </>
  )
}
export default App
